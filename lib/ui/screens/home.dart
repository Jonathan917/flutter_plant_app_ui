import 'package:flutter/material.dart';
import 'package:flutter_plant_app_ui/ui/widgets/cardFeaturedPlants.dart';
import 'package:flutter_plant_app_ui/ui/widgets/cardRecomended.dart';
import 'package:flutter_plant_app_ui/ui/widgets/titleWidget.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double searchDimension = (MediaQuery.of(context).size.width / 2) * 1.8;
    return SafeArea(
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  buildHeader(context, searchDimension),
                  buildRecomended(context),
                  buildFeaturedPlants(context),
                ],
              ),
            )));
  }

  Container buildFeaturedPlants(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TitleWidget(
                  title: "Featured Plants",
                  width: 140.0,
                ),
                FlatButton(
                    color: Theme.of(context).primaryColor,
                    height: 25.0,
                    minWidth: 60,
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                    onPressed: () => null,
                    child: Text(
                      "More",
                      style: TextStyle(color: Colors.white),
                    ))
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CardFeaturedPlants(
                    urlImage: './assets/images/bottom_img_1.png',
                  ),
                  CardFeaturedPlants(
                    urlImage: './assets/images/bottom_img_2.png',
                  ),
                ],
              ),
            )
          ],
        ),
      );

  Container buildRecomended(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TitleWidget(
                  title: "Recomended",
                  width: 114.0,
                ),
                FlatButton(
                    color: Theme.of(context).primaryColor,
                    height: 25.0,
                    minWidth: 60,
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                    onPressed: () => null,
                    child: Text(
                      "More",
                      style: TextStyle(color: Colors.white),
                    ))
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CardRecomendedWidget(
                    name: "Samantha",
                    price: 400,
                    country: "Russia",
                    url: './assets/images/image_1.png',
                  ),
                  CardRecomendedWidget(
                    name: "Beathrice",
                    price: 540,
                    country: "France",
                    url: './assets/images/image_2.png',
                  ),
                  CardRecomendedWidget(
                    name: "John",
                    price: 320,
                    country: "Usa",
                    url: './assets/images/image_3.png',
                  )
                ],
              ),
            )
          ],
        ),
      );

  Column buildHeader(BuildContext context, double searchDimension) => Column(
        children: [
          Container(
            height: 150,
            child: Stack(children: [
              Container(
                height: 130,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40), bottomRight: Radius.circular(40)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20, left: 20, top: 10.0),
                child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Text(
                    "Hi Jonathan",
                    style: TextStyle(color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.w700),
                  ),
                  CircleAvatar(
                    backgroundImage: AssetImage('./assets/images/logo.png'),
                    backgroundColor: Colors.transparent,
                    radius: 30,
                  )
                ]),
              ),
              Positioned(
                bottom: 0,
                left: (MediaQuery.of(context).size.width - searchDimension) / 2,
                child: Container(
                  height: 50.0,
                  width: searchDimension,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(13)),
                    boxShadow: [
                      new BoxShadow(color: Theme.of(context).textSelectionHandleColor, blurRadius: 15.0, offset: Offset(1, 5)),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Form(
                            child: SizedBox(
                          width: 100.0,
                          child: TextFormField(
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500, color: Theme.of(context).textSelectionHandleColor),
                            decoration: InputDecoration(
                                hintText: "Search", border: InputBorder.none, hintStyle: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500, color: Theme.of(context).textSelectionHandleColor)),
                          ),
                        )),
                        Icon(
                          Icons.search,
                          size: 30,
                          color: Theme.of(context).textSelectionHandleColor,
                        )
                      ],
                    ),
                  ),
                ),
              )
            ]),
          )
        ],
      );
}
