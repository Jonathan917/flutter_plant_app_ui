import 'package:flutter/material.dart';
import 'favourite.dart';
import 'home.dart';
import 'profile.dart';

class BottomNavScreen extends StatefulWidget {
  @override
  _BottomNavScreenState createState() => _BottomNavScreenState();
}

class _BottomNavScreenState extends State<BottomNavScreen> {
  final List _screen = [HomeScreen(), FavouriteScreen(), ProfileScreen()];
  int _currentIndex = 0;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        body: _screen[_currentIndex],
        drawer: new Drawer(),
        appBar: AppBar(
          elevation: 0.0,
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              _drawerKey.currentState.openDrawer();
            },
          ),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Color.fromARGB(255, 245, 245, 245),
            currentIndex: _currentIndex,
            type: BottomNavigationBarType.fixed,
            elevation: 0,
            iconSize: 25.0,
            selectedItemColor: Theme.of(context).primaryColor,
            unselectedItemColor: Colors.grey[400],
            showSelectedLabels: false,
            showUnselectedLabels: false,
            onTap: (index) => setState(() => _currentIndex = index),
            items: [Icons.home, Icons.favorite_outline, Icons.face]
                .asMap()
                .map((key, value) => MapEntry(
                    key,
                    BottomNavigationBarItem(
                        label: '',
                        icon: Container(
                          child: Icon(value),
                        ))))
                .values
                .toList()));
  }
}
