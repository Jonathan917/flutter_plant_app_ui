import 'package:flutter/material.dart';
import 'package:flutter_plant_app_ui/ui/widgets/iconInformation.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class DetailScreen extends StatelessWidget {
  final String _name;
  final String _country;
  final int _price;
  final List<String> _urls;

  const DetailScreen({
    Key key,
    @required String name,
    @required String country,
    @required int price,
    @required List<String> urls,
  })  : _name = name,
        _country = country,
        _price = price,
        _urls = urls,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          color: Colors.grey[800],
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back),
        ),
        actions: [IconButton(color: Colors.grey[800], icon: Icon(Icons.more_horiz), onPressed: () => {})],
      ),
      body: Column(
        children: [builderHeader(context), Expanded(child: buildContent(context))],
      ),
    );
  }

  Padding buildContent(BuildContext context) => Padding(
        padding: EdgeInsets.only(top: 50.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        _name,
                        style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.w700),
                      ),
                      Text(
                        "\$$_price",
                        style: TextStyle(fontSize: 20.0, color: Theme.of(context).primaryColor),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: Text(
                    _country,
                    style: TextStyle(color: Theme.of(context).textSelectionHandleColor, fontSize: 18, fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 2,
                  height: 70.0,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(topRight: Radius.circular(30)),
                  ),
                  child: Center(
                    child: Text(
                      "Buy Now",
                      style: TextStyle(fontWeight: FontWeight.w700, color: Colors.white),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: Center(
                      child: Text(
                    "Description",
                    style: TextStyle(fontWeight: FontWeight.w700),
                  )),
                )
              ],
            ),
          ],
        ),
      );

  Container builderHeader(BuildContext context) {
    final double height = MediaQuery.of(context).size.height / 1.3;
    final double width = MediaQuery.of(context).size.width / 1.35;
    return Container(
      height: height,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          Positioned(
              left: ((MediaQuery.of(context).size.width - width) / 2) - 30,
              bottom: 0 + 80.0,
              child: IconInformationWidget(
                iconData: Icons.brightness_7,
              )),
          Positioned(
              left: ((MediaQuery.of(context).size.width - width) / 2) - 30,
              bottom: (height / 4) + 50,
              child: IconInformationWidget(
                iconData: Icons.waves,
              )),
          Positioned(
              left: ((MediaQuery.of(context).size.width - width) / 2) - 30,
              bottom: (height / 4) * 2 + 25,
              child: IconInformationWidget(
                iconData: Icons.device_thermostat,
              )),
          Positioned(left: ((MediaQuery.of(context).size.width - width) / 2) - 30, bottom: (height / 4) * 3, child: IconInformationWidget(iconData: Icons.nights_stay_outlined)),
          Positioned(
            right: 0,
            top: 0,
            child: Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                color: Colors.transparent,
                boxShadow: [
                  new BoxShadow(color: Theme.of(context).textSelectionHandleColor, blurRadius: 20.0, offset: Offset(-1, 5)),
                ],
              ),
              child: Swiper(
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(53)),
                      image: DecorationImage(image: AssetImage(_urls[index]), fit: BoxFit.fill),
                    ),
                  );
                },
                itemCount: _urls.length,
                pagination: new SwiperPagination(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
