import 'package:flutter/material.dart';
import 'package:flutter_plant_app_ui/ui/screens/bottomNavScreen.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData(primaryColor: Color.fromARGB(255, 26, 153, 105), backgroundColor: Color.fromARGB(255, 249, 248, 253), textSelectionHandleColor: Color.fromARGB(255, 179, 224, 219)),
      routes: {},
      // AuthBlocProvider(child: RegisterScreen())},
      // onUnknownRoute: (RouteSettings setting) {
      //   return new MaterialPageRoute(builder: (context) => BottomNavScreen());
      // },
      home: BottomNavScreen(),
    );
  }
}
