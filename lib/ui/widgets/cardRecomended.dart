import 'package:flutter/material.dart';
import 'package:flutter_plant_app_ui/ui/screens/detail.dart';

class CardRecomendedWidget extends StatelessWidget {
  final String _name;
  final String _country;
  final int _price;
  final String _url;

  const CardRecomendedWidget({
    Key key,
    @required String name,
    @required String country,
    @required int price,
    @required String url,
  })  : _name = name,
        _country = country,
        _price = price,
        _url = url,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> _imagesUrl = ['./assets/images/img.png', './assets/images/img.png', './assets/images/img.png'];
    return GestureDetector(
      onTap: () => {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailScreen(
                      name: _name,
                      country: _country,
                      price: _price,
                      urls: _imagesUrl,
                    )))
      },
      child: Container(
        margin: EdgeInsets.only(top: 5, right: 20, bottom: 20),
        padding: EdgeInsets.all(0),
        height: 230.0,
        width: 150.0,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            new BoxShadow(color: Theme.of(context).textSelectionHandleColor, blurRadius: 10.0, offset: Offset(1, 4)),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image(
              height: 170,
              width: 150,
              fit: BoxFit.fill,
              image: AssetImage(_url),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 5, left: 5, top: 10, bottom: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    _name.toUpperCase(),
                    style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w500),
                  ),
                  Text('\$$_price', style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 13, fontWeight: FontWeight.w700))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 5.0, left: 5.0),
              child: Text(
                _country.toUpperCase(),
                style: TextStyle(color: Theme.of(context).textSelectionHandleColor, fontSize: 13),
              ),
            )
          ],
        ),
      ),
    );
  }
}
