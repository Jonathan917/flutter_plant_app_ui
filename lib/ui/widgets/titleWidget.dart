import 'dart:ffi';

import 'package:flutter/material.dart';

class TitleWidget extends StatelessWidget {
  final String _title;
  final double _width;
  const TitleWidget({
    @required String title,
    @required double width,
    Key key,
  })  : _title = title,
        _width = width,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(overflow: Overflow.visible, children: [
      Positioned(
        top: 15,
        left: -3,
        child: Container(
          height: 5,
          width: _width,
          decoration: BoxDecoration(color: Theme.of(context).textSelectionHandleColor),
        ),
      ),
      Text(
        _title,
        style: TextStyle(
          fontWeight: FontWeight.w800,
          fontSize: 18,
        ),
      ),
    ]);
  }
}
