import 'package:flutter/material.dart';

class CardFeaturedPlants extends StatelessWidget {
  final String _urlImage;

  const CardFeaturedPlants({
    Key key,
    @required String urlImage,
  })  : _urlImage = urlImage,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5, right: 20, bottom: 20),
      padding: EdgeInsets.all(0),
      height: 200.0,
      width: 250.0,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          new BoxShadow(color: Theme.of(context).textSelectionHandleColor, blurRadius: 10.0, offset: Offset(1, 4)),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Image(
            height: 200,
            fit: BoxFit.fill,
            image: AssetImage(_urlImage),
          ),
        ],
      ),
    );
  }
}
