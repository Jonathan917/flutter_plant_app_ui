import 'package:flutter/material.dart';

class IconInformationWidget extends StatelessWidget {
  final IconData _iconData;

  const IconInformationWidget({
    @required IconData iconData,
    Key key,
  })  : _iconData = iconData,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: 60,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          new BoxShadow(color: Theme.of(context).textSelectionHandleColor, blurRadius: 15.0, offset: Offset(1, 5)),
        ],
      ),
      child: Icon(
        _iconData,
        color: Theme.of(context).primaryColor,
      ),
    );
  }
}
